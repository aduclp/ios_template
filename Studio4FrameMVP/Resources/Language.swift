//
//  Language.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 9/11/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

class TextGlobal {
    static var  Welcome : String  { return "Welcome".localized }
    
    //MARK: CONTROL    
    static var  BUTTON_OK : String { return "BUTTON_OK".localized }
    static var  BUTTON_CANCEL : String {return "BUTTON_CANCEL".localized }
    
    //MARK: MESSAGE COMMON
    static var  MESSAGE_NO_INTERNET : String { return "MESSAGE_NO_INTERNET".localized }
    static var  MESSAGE_ERROR_URL : String {return "MESSAGE_ERROR_URL".localized }
    static var  MESSAGE_ERROR_UNKNOWN : String {return "MESSAGE_ERROR_UNKNOWN".localized }
    static var  MESSAGE_ERROR_OBJECT_SERICALIZATION : String {return "MESSAGE_ERROR_OBJECT_SERICALIZATION".localized }
    static var  MESSAGE_ERROR_PARSE_JSON : String {return "MESSAGE_ERROR_PARSE_JSON".localized }
    static var  MESSAGE_ERROR_CALL_SERVICE : String { return "MESSAGE_ERROR_CALL_SERVICE".localized }
}
