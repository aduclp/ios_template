//
//  ListUserVC.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ListUserVC: BaseVC {

    //MARK: - IBOutlet
    @IBOutlet weak var lbExample: UILabel!
    @IBOutlet weak var lbWelcome : UILabel!
    
    //MARK: - Properties
    private  let presenter = UserPresenter()
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
       setupData()
    }
    
    //MARK: - Initialization Method
    private func setupData(){
        self.lbWelcome.text = TextGlobal.Welcome
        let ref = Database.database().reference()
        ref.child("SomeId/Name").setValue("An1")
//        ref.childByAutoId().setValue(["Name":"AA","role":"admin","age":39])
        ref.child("users").child("SomeId").setValue(["Name": "Test2"])
    }
    
    //MARK: - Private Method
    private func isValidate() -> Bool {
        if !self.isNetworkAvailable() {
            return false
        }
        return true
    }
    
    //MARK: - Public Method
    
    //MARK: - Target
    
    //MARK: - IBAction
    @IBAction func sendAPI(_ sender: Any) {
       serviceGetListUser()
    }
}
//MARK: - Data
extension ListUserVC : UserDelegate {
 
    //MARK: - Get List
    private func serviceGetListUser(){
        if self.isValidate() {
            presenter.delegate = self
            self.showProgress(true)
            presenter.getListUser()
        }
    }
    
    //MARK: - UserDelegate
    func getUserSuccess(listUser : [User]?) {
        self.showProgress(false)
        
        guard let listUser = listUser, listUser.count > 0  else {
            return
        }
        
        let firstUser = listUser[0];
        lbExample.text = firstUser.title
    }
    
    func getUserError(error : String){
        self.showProgress(false)
        
        lbExample.text = error
        lbExample.textColor = UIColor(hexString: "#dc002f")
    }
}
