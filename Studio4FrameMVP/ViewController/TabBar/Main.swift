//
//  Main.swift
//  Studio4FrameMVP
//
//  Created by LeAnhDuc32 on 4/17/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

class Main: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
       super.viewDidAppear(animated)
       self.initTabBar()
    }
    
    public func initTabBar() {
        
        let tabBarController = UITabBarController()

        let favoritesVC = ContentVC()
        favoritesVC.title = "Favorites"
        favoritesVC.view.backgroundColor = UIColor.orange
        let downloadsVC = ContentVC()
        downloadsVC.title = "Downloads"
        downloadsVC.view.backgroundColor = UIColor.blue
        let historyVC = ContentVC()
        historyVC.title = "History"
        historyVC.view.backgroundColor = UIColor.cyan
        let testVC = ContentVC()
        testVC.title = "Test"
        testVC.view.backgroundColor = UIColor.cyan
        
        // Set the tabBarItem for each ViewController.
//
        favoritesVC.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 0)
        downloadsVC.tabBarItem = UITabBarItem(tabBarSystemItem: .downloads, tag: 1)
        historyVC.tabBarItem = UITabBarItem(tabBarSystemItem: .history, tag: 2)
        testVC.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 3)

//        Set the tabBarController’s viewControllers property to an array of our three viewControllers.
        
        let controllers = [favoritesVC, downloadsVC, historyVC, testVC]
        tabBarController.viewControllers = controllers
        
        //Create and set a UINavigationController for each viewController.
        tabBarController.viewControllers = controllers.map { UINavigationController(rootViewController: $0)}
        
        self.present(tabBarController, animated: true, completion: nil)
        
    }

}
