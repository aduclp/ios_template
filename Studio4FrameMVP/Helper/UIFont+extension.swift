//
//  UIFont+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 11/27/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

extension UIFont {
    
    static let largeTextFont = UIFont.defaultFont(isBold: true, fontSize: 15)
    static let smallTextFont = UIFont.defaultFont(isBold: false, fontSize: 10)
    
    class func defaultFont(isBold: Bool = false, fontSize: CGFloat = 13.0) ->UIFont {
        var destFont : String {
            return (isBold ? "W6" : "W3")
        }
        
        return UIFont.init(name: String(format: "HiraginoSans-%@",destFont), size: fontSize)!
    }
}
