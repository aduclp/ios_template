//
//  UILabel+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 10/31/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

extension UILabel {
    @IBInspectable
    var xibLockey : String? {
        get {
            return nil
        }
        set(key){
            text = key?.localized
        }
    }
    
    func setAttributed(string str: String, font: UIFont = UIFont.defaultFont(), foregroundColor: UIColor = UIColor(hexString: "#333333"),lineSpacing: CGFloat = 3, alignment: NSTextAlignment = .left) {
        //atributionString
        let textAtribution = [
            NSAttributedStringKey.foregroundColor : foregroundColor,
            NSAttributedStringKey.font : font
            ] as [NSAttributedStringKey : Any]
        let attrString = NSMutableAttributedString(string: str, attributes: textAtribution)
        //line spacing
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        attrString.addAttribute(NSAttributedStringKey.paragraphStyle,
                                value:paragraphStyle,
                                range:NSMakeRange(0, attrString.length))
        self.attributedText = attrString
        //alignment
        self.textAlignment = alignment
        //multi line
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
    }
}
