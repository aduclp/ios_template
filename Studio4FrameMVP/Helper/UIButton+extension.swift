//
//  UIButton+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 10/31/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

extension UIButton {
    @IBInspectable
    var xibLockey : String?{
        get {
            return nil
        }
        set(key){
            setTitle(key?.localized, for: .normal)
        }
    }
}

extension UIButton {
    func getAttributedStringUnderLine(foregroundColor: UIColor, font: UIFont = UIFont.defaultFont(isBold: false, fontSize: 12), title: String) -> NSMutableAttributedString {
        let underLineButtonAttributes : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font : font,
            NSAttributedStringKey.foregroundColor : foregroundColor,
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
        let attributeString = NSMutableAttributedString(string: title, attributes: underLineButtonAttributes)
        return attributeString
    }
}
