//
//  Date+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 8/30/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

extension Date {
    
    func getDate(string: String! ,format: String? = "yyyy/MM/dd")->Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        guard let date = dateFormatter.date(from: string) else {
            print("ERROR: Date conversion failed due to mismatched format.")
            return nil
        }
        return date
    }
    
    func getString(format: String?) -> String{
       let dateFormatter = DateFormatter()
        
        if let strFormat = format{
            dateFormatter.dateFormat = strFormat
        } else {
            dateFormatter.dateFormat = "yyyy/MM/dd"
        }
        
        let strDate = dateFormatter.string(from: self)
        
        return strDate
    }
    
    func countDay(dateBegin: Date, dateEnd: Date) -> Int?{
        let calendar = NSCalendar.current
        
        let date1 = calendar.startOfDay(for: dateBegin)
        let date2 = calendar.startOfDay(for: dateEnd)
        
        let component = calendar.dateComponents([.day], from: date1, to: date2)
        
        return component.day
    }
    
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    func getEndDate(addbyUnit:Calendar.Component, value : Int) -> Date {
        guard let endDate = Calendar.current.date(byAdding: addbyUnit, value: value, to: self) else { return self }
        return endDate
    }
    
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
}
