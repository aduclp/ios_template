//
//  String+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 8/30/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func trim() -> String{
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
            0x1F1E6...0x1F1FF: // Flags
                return true
            default:
                continue
            }
        }
        return false
    }
    
    var isNumber :Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func validate(regex: String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: self)
    }
    
    var isEmail1: Bool {
        return validate(regex: "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*")
    }
    
    var isEmail2: Bool {
        return validate(regex: "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}")
    }
    
    var isKatakana: Bool {
        return validate(regex: "^[\\u30a0-\\u30ff]+$")
    }
    
    var isKatakanaWithSpaceFullWidth: Bool {
        return validate(regex: "[\\u30a0-\\u30ff　]*")
    }
    
    var isHiragana: Bool {
        return validate(regex: "^[ぁ-ゞ]+$")
    }
    
    var isZipCode: Bool {
        return validate(regex: "^([0-9]){3}([-])?([0-9]){4}$")
    }
    
    var isFullWidth: Bool {
        if self.contains(" ") {
            return false
        }
        return validate(regex: "[一-龯ぁ-んア-ンＡ-Ｚ０-９ー（）／．ー　]*")
    }
    
    var isHalfWidth: Bool {
        return self.range(of: "^[0-9a-zA-Z\\_]{1,}$", options: .regularExpression) != nil
    }
    
    //minhnp width of string
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func convertCurrency(currencySymbol: String? ) -> String {
        if let numberInt = Int64(self) {
            let number = NSNumber(value:numberInt )
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale(identifier: "vi_VN")
            if let currencySymbol = currencySymbol {
                formatter.currencySymbol = currencySymbol
            }
            
            if let numberString = formatter.string(from: number) {
                return numberString
            }
        }
        return self
    }
    
    func getAttributed(font: UIFont = UIFont.defaultFont(), foregroundColor: UIColor = UIColor(hexString: "#333333"),lineSpacing: CGFloat = 0, textAlign : NSTextAlignment = .left) -> NSMutableAttributedString {
        //atributionString
        let textAtribution = [
            NSAttributedStringKey.foregroundColor : foregroundColor,
            NSAttributedStringKey.font : font
            ] as [NSAttributedStringKey : Any]
        let attrString = NSMutableAttributedString(string: self, attributes: textAtribution)
        //line spacing
        if lineSpacing != 0 {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = lineSpacing
            if textAlign != .left{
                paragraphStyle.alignment = textAlign
            }
            attrString.addAttribute(NSAttributedStringKey.paragraphStyle,
                                    value:paragraphStyle,
                                    range:NSMakeRange(0, attrString.length))
        }
        return  attrString
    }
    
    func getFormatYearBasedOnCalendarJapanese() -> (yearUS: String?, japaneseYearCharacter: String?, japaneseYear: String?) {
        //2018/11/11
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        let dateUS = dateFormatter.date(from: self)
        
        dateFormatter.dateFormat = "yyyy"
        let yearUS = dateFormatter.string(from: dateUS ?? Date())
        
        let f = DateFormatter()
        f.calendar = Calendar(identifier: .japanese)
        f.dateStyle = .long
        f.locale = Locale(identifier: "en_US@calendar=japanese")
        let strDateJP = f.string(from: dateUS ?? Date())
        
        // November 11, 30 Heisei
        // get year and name: ex: H31 -> H: name, 30: year japanese
        
        let arrStrDateJP = strDateJP.components(separatedBy: " ")
        
        var yearValue = ""
        var firstNameValue = ""
        
        if arrStrDateJP.count == 4 {
            yearValue = arrStrDateJP[2]
            let nameValue = arrStrDateJP[3]
            firstNameValue = String(nameValue[nameValue.index(nameValue.startIndex, offsetBy: 0)])
        }
        return (yearUS, firstNameValue, yearValue)
    }

}

extension String {
    //right is the first encountered string after left
    func between(_ left: String, _ right: String) -> String? {
        guard
            let leftRange = range(of: left), let rightRange = range(of: right, options: .backwards)
            , leftRange.upperBound <= rightRange.lowerBound
            else { return nil }
        
        let sub = self[leftRange.upperBound...]
        let closestToLeftRange = sub.range(of: right)!
        return String(sub[..<closestToLeftRange.lowerBound])
    }
    
    func substring(to : Int) -> String {
        let toIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[...toIndex])
    }
    
    func substring(from : Int) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: from)
        return String(self[fromIndex...])
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
    
    func character(_ at: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: at)]
    }
    
    func lastIndexOfCharacter(_ c: Character) -> Int? {
        return range(of: String(c), options: .backwards)?.lowerBound.encodedOffset
    }
    
    //SubString after text
    func subString(from text: String) -> String? {
        guard let range = self.range(of: text) else { return nil }
        return String(self[range.upperBound...])
    }
}
