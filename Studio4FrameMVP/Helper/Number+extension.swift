//
//  Int+extension.swift
//  IDOMFleaMarket
//
//  Created by manh.nq on 1/1/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import Foundation
import UIKit

extension Int {
    func decimal() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}

extension CGFloat {
    func rounded(places: Int) -> CGFloat {
        let divisor = pow(10.0, CGFloat(places))
        return (self * divisor).rounded() / divisor
    }
}
