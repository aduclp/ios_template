//
//  Alert+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 9/17/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(_ title: String?, _ message: String?, titleButtonDone: String?, titleButtonCancel: String? = TextGlobal.BUTTON_CANCEL, DoneAction: (() -> Void)? = nil , CancelAction: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // cancel button
        if let titleCancel = titleButtonCancel {
            let actionCancel = UIAlertAction(title: titleCancel, style: .default) { (action: UIAlertAction) in
                if let CancelHandler = CancelAction {
                    CancelHandler()
                }
            }
            alertController.addAction(actionCancel)
        }
        
        // done button
        if let titleDone = titleButtonDone {
            let actionDone = UIAlertAction(title: titleDone, style: .default) { (action: UIAlertAction) in
                if let DoneHandler = DoneAction {
                     DoneHandler()
                }
            }
            alertController.addAction(actionDone)
        }
        
        // present
        self.present(alertController, animated: true, completion: nil)
    }
    
    var SIAppDelegate : AppDelegate {
        return  UIApplication.shared.delegate as! AppDelegate
    }
}
