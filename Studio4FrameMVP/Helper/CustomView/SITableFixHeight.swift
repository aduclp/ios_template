//
//  IGTableFixHeight.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 3/9/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

//author: norbDEV
class SITableFixHeight: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIViewNoIntrinsicMetric, height: contentSize.height)
    }
}
