//
//  SITextField.swift
//  IDOMFleaMarket
//
//  Created by Nguyen Quang Manh on 1/10/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import Foundation
import UIKit

class SITextField: UITextField {
    
    @IBInspectable var insetX: CGFloat = 21 {
        didSet {
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var insetY: CGFloat = 6 {
        didSet {
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var isRequire: Bool = false {
        didSet {
            updateRequireUI()
        }
    }
    
    override var text: String? {
        didSet {
            updateRequireUI()
        }
    }
    
    //MARK: - Initialization Method
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    //MARK: - Private Method
    private func initView() {
        borderWidth = 1
        borderColor = AppColor.placeHolderColor
        borderStyle = .none
        textColor = AppColor.TextMainColor
        font = UIFont.defaultFont(isBold: false, fontSize: 12)
    }
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    private func updateRequireUI() {
        if let text = text, text.count > 0 {
            backgroundColor = AppColor.backgroudTextInputColor
            borderColor = AppColor.placeHolderColor
        } else {
            backgroundColor = isRequire ? AppColor.requireBackgroundColor : AppColor.backgroudTextInputColor
            borderColor = isRequire ? AppColor.requireBorderColor : AppColor.placeHolderColor
        }
    }
}

extension SITextField: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let SITextField = textField as? SITextField else { return }
        SITextField.updateRequireUI()
    }
}
