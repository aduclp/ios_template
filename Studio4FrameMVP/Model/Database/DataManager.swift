//
//  DataManager.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 9/18/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import RealmSwift

class DataManager: NSObject {

    static let shared = DataManager()
    private override init() {
        let config = Realm.Configuration(objectTypes: [MakerInfo.self])
        Realm.Configuration.defaultConfiguration = config
    }
    
    func addOrUpdate(object : Object,completionBlock: @escaping () -> Void,fail: @escaping (NSError) -> Void){
        DispatchQueue(label: AppKey.queuerealmgulliver).async {
            do {
                let realm = try Realm()
                try realm.write {
                    realm.add(object, update: true)
                }
                
                completionBlock()
            } catch let error {
                fail(error as NSError)
            }
        }
    }
    
    
    func addOrUpdate(listObject : [Object] ,completionBlock: @escaping () -> Void,fail: @escaping (NSError) -> Void){
        DispatchQueue(label: AppKey.queuerealmgulliver).async {
            do {
                let realm = try Realm()
                try listObject.forEach({ (object) in
                    try realm.write {
                        realm.add(object, update: true)
                    }
                })
                
                completionBlock()
            } catch let error {
                fail(error as NSError)
            }
        }
    }
    
    func query<T : Object>(type: T.Type,predicate : NSPredicate? = nil, sortby keypath: String? = nil) -> [T]{
            guard let realm = try? Realm() else { return [T]() }
            var result =  realm.objects(type)
            
            if  predicate != nil {
                result =  result.filter(predicate!)
            }
            
            if  keypath != nil {
                result = result.sorted(byKeyPath: keypath!)
            }
            
            let list  = result.toArray(type) as [T]
            return (list.count > 0 ) ? list : [T]()
    }
   
    func delete(object : Object, completion: @escaping (() -> Void), fail : @escaping ((NSError) -> Void)){
        DispatchQueue(label: AppKey.queuerealmgulliver).async {
            do {
                let realm = try Realm()
                try realm.write {
                    realm.delete(object)
                }
                
                completion()
            } catch let error{
                fail(error as NSError)
            }
        }
    }
    
    func delete(listObject : List<Object>, completion:  @escaping () -> Void, fail :  @escaping (NSError) -> Void){
        DispatchQueue(label: AppKey.queuerealmgulliver).async {
            do {
                let realm = try Realm()
                try realm.write {
                    for object in listObject {
                        realm.delete(object)
                    }
                }
            } catch let error {
                fail(error as NSError)
            }
        }
    }
    //warning: not using/ (delete all database)
    func deleteAll(){
        DispatchQueue(label: AppKey.queuerealmgulliver).async {
            guard let realm = try? Realm() else { return }
            try! realm.write {
                realm.deleteAll()
            }
        }
    }
    
}
