//
//  MakerInfo.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 12/28/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import RealmSwift

class MakerInfo : Object {
    @objc dynamic var id: String = UUID().uuidString.lowercased()
    @objc dynamic var makerCd: String = ""
    @objc dynamic var makerName: String = ""
    @objc dynamic var makerCategoryId: Int = 0
    
    override static func primaryKey() -> String? {
        return "makerCd"
    }
}

