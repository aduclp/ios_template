//
//  Results+extension.swift
//  RepairMotorSupport
//
//  Created by quy nguyen on 11/8/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import RealmSwift

extension Results {
    func toArray<T>(_ ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0..<count{
            if let obj = self[i] as? T {
                array.append(obj)
            }
        }
        return array
    }
}

extension List {
    func toArray<T>(_ element: T.Type) -> [T] {
        var array = [T]()
        for i in 0..<count{
            if let obj = self[i] as? T {
                array.append(obj)
            }
        }
        return array
    }
}
