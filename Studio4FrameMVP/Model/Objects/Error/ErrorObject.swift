//
//  ErrorObject.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 12/26/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

class ErrorObject: NSObject, Codable {
    let message: String?
    let errors: [APIError]?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case errors = "errors"
    }
    
    init(message: String, errors: [APIError]) {
        self.message = message
        self.errors = errors
    }
}

class APIError: Codable {
    let field: String?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case field = "field"
        case message = "message"
    }
    
    init(field: String, message: String) {
        self.field = field
        self.message = message
    }
}
