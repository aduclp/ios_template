//
//  ListUserResponse.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

class User: NSObject, Codable {
    var userId: Int?
    var title: String?
    var completed: Bool?
    
    private enum CodingKeys : String, CodingKey {
        case userId = "user_id"
        case title
        case completed
    }
    
    init(userId: Int?, title: String?, completed: Bool?) {
        self.userId = userId
        self.title = title
        self.completed = completed
    }
    
//    required init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.userId = try container.decode(Int.self, forKey: .userId)
//        self.title = try container.decode(String.self, forKey: .title)
//        self.completed = try container.decode(Bool.self, forKey: .completed)
//
//        try super.init(from: decoder)
//    }
    
}

