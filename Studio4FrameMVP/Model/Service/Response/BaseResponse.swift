//
//  BaseResponse.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

class BaseResonse: NSObject, Codable {
       var id: Int = 0
    
    private enum CodingKeys: String, CodingKey {
        case id
    }
    
     required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
    }

}
