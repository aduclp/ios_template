//
//  AppURL.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation


struct AppURL {
    
    //example
    static let BaseUrl = "https://jsonplaceholder.typicode.com/todos"
    
    static let HOST_URL   = "https://api-dev.dmma.whotv.net/"
    static let BASE_URL   = "api/v1/"
    
    static func LOGIN_API(email: String, password: String) ->String {
        return "\(HOST_URL)\(BASE_URL)login?email=\(email)&password=\(password)"
    }
    
}



