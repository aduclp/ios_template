//
//  NetworkUtils.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/22/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit


//typealias CompletionBlock = (_ response : [AnyObject])  -> Void
//typealias FailBlock = (_ error : Error) -> Void

class NetworkUtils: NSObject {
    
    static let shared = NetworkUtils()
    fileprivate var timeout : TimeInterval = 60
    fileprivate var defaultHeader : [String : String]? = [:]
    fileprivate let queue = OperationQueue()
    
    private override init() {
        queue.name = "studio4"
        queue.maxConcurrentOperationCount = 1
    }
    
    //MARK: - Base Request
    public func request(url: String, method: REQUEST_TYPE , param: Data? , header: [String:String]? , taskId: String?, completion:(@escaping(Int,Data) -> Void), fail: (@escaping (ModelError) -> Void)){
        
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            let error = ModelError.URLError
            fail(error)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        
        switch method {
        case REQUEST_TYPE.REQUEST_TYPE_GET:
            urlRequest.httpMethod = "GET"
        case REQUEST_TYPE.REQUEST_TYPE_POST:
            urlRequest.httpMethod = "POST"
        case REQUEST_TYPE.REQUEST_TYPE_PUT:
            urlRequest.httpMethod = "PUT"
        case REQUEST_TYPE.REQUEST_TYPE_DELETE:
            urlRequest.httpMethod = "DELETE"
        }
        
        var headers = urlRequest.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        urlRequest.allHTTPHeaderFields = headers
        
        if let defaultHeader = defaultHeader {
            for(key, value) in defaultHeader{
                urlRequest .setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let headers = header {
            for (key, value) in headers{
                urlRequest .setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let bodyData = param {
            urlRequest.httpBody = bodyData
        }
        
        urlRequest.timeoutInterval = timeout
        
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            guard error == nil else {
                print("error calling API")
                print(error!)
                let errorService = ModelError.ServiceError
                fail(errorService)
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                let error = ModelError.ObjectSericalization
                fail(error)
                return
            }
            
            //response code
            let httpResponse = response as! HTTPURLResponse
            print("API: \(url) - http status code : \(httpResponse.statusCode)")
            
            completion(httpResponse.statusCode, responseData)
            
        }
        
        if  taskId != nil {
            queue.addOperation {
                task.resume()
            }
        } else {
            task.resume()
        }
        
    }
    
    //MARK: - Support Method
    public func setDefaultHeader(_ header: [String:String]? ){
        defaultHeader = header;
    }
    
    public func cancellAllOperation(){
        queue.cancelAllOperations()
    }
    
    public func setTimeOut(_ time: TimeInterval){
        timeout = time
    }
    
    //MARK: - Upload Image
    func uploadImageRequest(url: String, method: REQUEST_TYPE = .REQUEST_TYPE_POST ,params: [String : Any] = [:], listData: [String : [(String, String, Data)]] = [:], files: [String : (String, URL)] = [:], header: [String:String]? , taskId: String?, completion:(@escaping(Int,Data) -> Void), fail: (@escaping (ModelError) -> Void))
    {
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            let error = ModelError.URLError
            fail(error)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        
        switch method {
        case REQUEST_TYPE.REQUEST_TYPE_GET:
            urlRequest.httpMethod = "GET"
        case REQUEST_TYPE.REQUEST_TYPE_POST:
            urlRequest.httpMethod = "POST"
        case REQUEST_TYPE.REQUEST_TYPE_PUT:
            urlRequest.httpMethod = "PUT"
        case REQUEST_TYPE.REQUEST_TYPE_DELETE:
            urlRequest.httpMethod = "DELETE"
        }
    
        if let defaultHeader = defaultHeader {
            for(key, value) in defaultHeader{
                urlRequest .setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let headers = header {
            for (key, value) in headers{
                urlRequest .setValue(value, forHTTPHeaderField: key)
            }
        }
        
        var headers = urlRequest.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.setValue("multipart/form-data; boundary=\(NetworkUtils.boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = NetworkUtils.multiPartFormData(boundary: NetworkUtils.boundary, params: params, listData: listData, files: files)
        
        
        urlRequest.timeoutInterval = timeout
        
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            guard error == nil else {
                print("error calling API")
                print(error!)
                let errorService = ModelError.ServiceError
                fail(errorService)
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                let error = ModelError.ObjectSericalization
                fail(error)
                return
            }
            
            //response code
            let httpResponse = response as! HTTPURLResponse
            print("http status code : \(httpResponse.statusCode)")
            
            completion(httpResponse.statusCode, responseData)
            
        }
        
        if  taskId != nil {
            queue.addOperation {
                task.resume()
            }
        } else {
            task.resume()
        }
    }
    
    class func multiPartFormData(boundary: String, params: [String : Any], listData: [String : [(String, String, Data)]], files: [String : (String, URL)]) -> Data {
        var data = Data()
        for param in params {
            if let array = param.value as? [Any] {
                array.forEach {
                    data.append("--\(boundary)\r\n".data(using: .utf8)!)
                    data.append("Content-Disposition: form-data; name=\"\(param.key)\"\r\n\r\n".data(using: .utf8)!)
                    data.append("\($0)\r\n".data(using: .utf8)!)
                }
            } else {
                data.append("--\(boundary)\r\n".data(using: .utf8)!)
                data.append("Content-Disposition: form-data; name=\"\(param.key)\"\r\n\r\n".data(using: .utf8)!)
                data.append("\(param.value)\r\n".data(using: .utf8)!)
            }
        }
        for dataInfo in listData {
            let key = dataInfo.key
            let array = dataInfo.value
            for value in array {
                let mimeType = value.0
                let name = value.1
                let fileData = value.2
                data.append("--\(boundary)\r\n".data(using: .utf8)!)
                data.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(name)\"\r\n".data(using: .utf8)!)
                data.append("Content-Type: \(mimeType)\r\n\r\n".data(using: .utf8)!)
                data.append(fileData)
                data.append("\r\n".data(using: .utf8)!)
            }
        }
        for file in files {
            do {
                let key = file.key
                let mimeType = file.value.0
                let url = file.value.1
                let fileData = try Data(contentsOf: url)
                data.append("--\(boundary)\r\n".data(using: .utf8)!)
                data.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(url.lastPathComponent)\"\r\n".data(using: .utf8)!)
                data.append("Content-Type: \(mimeType)\r\n\r\n".data(using: .utf8)!)
                data.append(fileData)
                data.append("\r\n".data(using: .utf8)!)
            } catch { }
        }
        data.append("--\(boundary)--\r\n".data(using: .utf8)!)
        return data
    }
    
    class var boundary: String {
        return "Jimoful\(Int(Date().timeIntervalSince1970))"
    }
}


