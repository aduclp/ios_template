//
//  UserPresenter.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

class UserPresenter: BasePresenter {
    weak var delegate : UserDelegate?
    
    public func getAllUser(){
        
    }
    
    public func getListUser(){
        NetworkUtils.shared.request(url: AppURL.BaseUrl, method: .REQUEST_TYPE_GET, param: nil, header: nil, taskId: nil, completion: { [weak self] (statusCode, data) in
            guard let `self` = self else {
                return
            }
            let response = self.processResponseData(statusCode: statusCode, data: data, objectConvert: [User].self)
            let errorMessage = response.errorMessage
            let responseData = response.responseData
            
            print(data)
            
            if let errorMessage = errorMessage, !errorMessage.isEmpty {
                //api response fail
                DispatchQueue.main.async {
                    self.delegate?.getUserError(error: errorMessage)
                }
            } else {
                //api response success
                DispatchQueue.main.async {
                    self.delegate?.getUserSuccess(listUser: responseData)
                }
            }
            
        }) {  [weak self] (error) in
            guard let `self` = self else {
                return
            }
            DispatchQueue.main.async {
                self.delegate?.callServiceFail(error: error)
            }
        }
    }

}
