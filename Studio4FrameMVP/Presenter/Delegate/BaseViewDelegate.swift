//
//  BaseViewDelegate.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 12/26/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

protocol BaseViewDelegate : class {
    func callServiceFail(error: ModelError)
}

extension BaseViewDelegate {
    public func callServiceFail(error: ModelError){}
}
