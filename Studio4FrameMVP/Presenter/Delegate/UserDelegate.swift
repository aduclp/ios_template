//
//  UserDelegate.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation


protocol UserDelegate : BaseViewDelegate {
    func getUserSuccess(listUser : [User]?)
    func getUserError(error : String)
}

extension UserDelegate {
    public func getUserSuccess(listUser : [User]?){}
    public func getUserError(error : String){}
}
