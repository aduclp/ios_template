//
//  BasePresenter.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 12/26/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation
import UIKit

protocol BasePresenterProtocol : class {
    func processResponseData<T : Codable>(statusCode : Int, data: Data, objectConvert: T.Type) -> (errorMessage: String?, responseData: T?)
    func addUserTokenHeader() -> [String: String]
}

class BasePresenter: NSObject, BasePresenterProtocol {
    
    public func processResponseData<T : Codable>(statusCode : Int, data: Data, objectConvert: T.Type) -> (errorMessage: String?, responseData: T?){
        if String(statusCode).hasPrefix("2") {  //success status code : 2xx
            //success
            let decoder = JSONDecoder()
            do{
                let responseData = try decoder.decode(objectConvert, from: data)
                return (nil, responseData)
            }
            catch let error {
                print(error)
                let errorMessage = ModelError.ParseJson.localizedDescription
                return (errorMessage, nil)
            }
        } else {
            //fail
            let errorMessage = self.getMessageError(data: data)
            return (errorMessage, nil)
        }
    }
    
    public func addUserTokenHeader() -> [String: String] {
        let userToken = UserDefaults.standard.string(forKey: AppKey.UserToken) ?? ""
        return ["Authorization" : userToken]
    }
    
    public func getMessageError(data: Data) -> String{
        var errorMessage = "error"
        let decoder = JSONDecoder()
        do{
             let errorObj = try decoder.decode(ErrorObject.self, from: data)
             //get messages in array errors
            if let errors = errorObj.errors, errors.count > 0 {
                let error = errors[0]
                errorMessage = error.message ?? "error"
            } else
                if let errMessage = errorObj.message, !errMessage.isEmpty {
                    errorMessage = errMessage
                }
            return errorMessage
            
        }catch let error {
            print(error)
            errorMessage = ModelError.ParseJson.localizedDescription
            return errorMessage
        }
    }
}
