//
//  AppColor.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation
import UIKit

class AppColor {
    static let BackgroundColor : UIColor = UIColor(hexString: "#00a0dc")
    static let MainColor : UIColor = UIColor(hexString: "#008D4C")
    static let TextMainColor : UIColor = UIColor(hexString: "#333333")
    static let placeHolderColor: UIColor = UIColor(hexString: "#DDDDDD")
    static let backgroudTextInputColor: UIColor = UIColor(hexString: "#FDFCFA")
    static let requireBackgroundColor: UIColor = UIColor(hexString: "#FFFFF5")
    static let requireBorderColor: UIColor = UIColor(hexString: "#00A0E9")
}
