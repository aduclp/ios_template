//
//  Enum.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

enum REQUEST_TYPE : Int {
    case REQUEST_TYPE_GET = 1
    , REQUEST_TYPE_POST
    , REQUEST_TYPE_PUT
    , REQUEST_TYPE_DELETE
}

enum ModelError: Int ,Error {
    case URLError = 400
    case Unknown  = 404
    case NoInternet = 401
    case ObjectSericalization = 405
    case ParseJson = 505
    case ServiceError = 500
    
    var localizedDescription: String {
        switch self {
        case .URLError:
            return TextGlobal.MESSAGE_ERROR_URL
        case .Unknown:
            return TextGlobal.MESSAGE_ERROR_UNKNOWN
        case .NoInternet:
            return TextGlobal.MESSAGE_NO_INTERNET
        case .ObjectSericalization:
            return TextGlobal.MESSAGE_ERROR_OBJECT_SERICALIZATION
        case .ParseJson:
            return TextGlobal.MESSAGE_ERROR_PARSE_JSON
        case .ServiceError:
            return TextGlobal.MESSAGE_ERROR_CALL_SERVICE
        }
    }
    var _code : Int {
        return self.rawValue
    }
}
